/* AUTOGENERATED FILE. DO NOT EDIT. */
#ifndef _MOCK_XT_IOPORTS_H
#define _MOCK_XT_IOPORTS_H

#include "unity.h"
#include "stdint.h"
#include "xt_ioports.h"

/* Ignore the following warnings, since we are copying code */
#if defined(__GNUC__) && !defined(__ICC) && !defined(__TMS470__)
#if __GNUC__ > 4 || (__GNUC__ == 4 && (__GNUC_MINOR__ > 6 || (__GNUC_MINOR__ == 6 && __GNUC_PATCHLEVEL__ > 0)))
#pragma GCC diagnostic push
#endif
#if !defined(__clang__)
#pragma GCC diagnostic ignored "-Wpragmas"
#endif
#pragma GCC diagnostic ignored "-Wunknown-pragmas"
#pragma GCC diagnostic ignored "-Wduplicate-decl-specifier"
#endif

void mock_xt_ioports_Init(void);
void mock_xt_ioports_Destroy(void);
void mock_xt_ioports_Verify(void);




#define WUR_EXPSTATE_Ignore() WUR_EXPSTATE_CMockIgnore()
void WUR_EXPSTATE_CMockIgnore(void);
#define WUR_EXPSTATE_StopIgnore() WUR_EXPSTATE_CMockStopIgnore()
void WUR_EXPSTATE_CMockStopIgnore(void);
#define WUR_EXPSTATE_ExpectAnyArgs() WUR_EXPSTATE_CMockExpectAnyArgs(__LINE__)
void WUR_EXPSTATE_CMockExpectAnyArgs(UNITY_LINE_TYPE cmock_line);
#define WUR_EXPSTATE_Expect(v) WUR_EXPSTATE_CMockExpect(__LINE__, v)
void WUR_EXPSTATE_CMockExpect(UNITY_LINE_TYPE cmock_line, unsigned int v);
typedef void (* CMOCK_WUR_EXPSTATE_CALLBACK)(unsigned int v, int cmock_num_calls);
void WUR_EXPSTATE_AddCallback(CMOCK_WUR_EXPSTATE_CALLBACK Callback);
void WUR_EXPSTATE_Stub(CMOCK_WUR_EXPSTATE_CALLBACK Callback);
#define WUR_EXPSTATE_StubWithCallback WUR_EXPSTATE_Stub
#define WUR_EXPSTATE_IgnoreArg_v() WUR_EXPSTATE_CMockIgnoreArg_v(__LINE__)
void WUR_EXPSTATE_CMockIgnoreArg_v(UNITY_LINE_TYPE cmock_line);
#define WRMSK_EXPSTATE_Ignore() WRMSK_EXPSTATE_CMockIgnore()
void WRMSK_EXPSTATE_CMockIgnore(void);
#define WRMSK_EXPSTATE_StopIgnore() WRMSK_EXPSTATE_CMockStopIgnore()
void WRMSK_EXPSTATE_CMockStopIgnore(void);
#define WRMSK_EXPSTATE_ExpectAnyArgs() WRMSK_EXPSTATE_CMockExpectAnyArgs(__LINE__)
void WRMSK_EXPSTATE_CMockExpectAnyArgs(UNITY_LINE_TYPE cmock_line);
#define WRMSK_EXPSTATE_Expect(v, mask) WRMSK_EXPSTATE_CMockExpect(__LINE__, v, mask)
void WRMSK_EXPSTATE_CMockExpect(UNITY_LINE_TYPE cmock_line, unsigned int v, uint32_t mask);
typedef void (* CMOCK_WRMSK_EXPSTATE_CALLBACK)(unsigned int v, uint32_t mask, int cmock_num_calls);
void WRMSK_EXPSTATE_AddCallback(CMOCK_WRMSK_EXPSTATE_CALLBACK Callback);
void WRMSK_EXPSTATE_Stub(CMOCK_WRMSK_EXPSTATE_CALLBACK Callback);
#define WRMSK_EXPSTATE_StubWithCallback WRMSK_EXPSTATE_Stub
#define WRMSK_EXPSTATE_IgnoreArg_v() WRMSK_EXPSTATE_CMockIgnoreArg_v(__LINE__)
void WRMSK_EXPSTATE_CMockIgnoreArg_v(UNITY_LINE_TYPE cmock_line);
#define WRMSK_EXPSTATE_IgnoreArg_mask() WRMSK_EXPSTATE_CMockIgnoreArg_mask(__LINE__)
void WRMSK_EXPSTATE_CMockIgnoreArg_mask(UNITY_LINE_TYPE cmock_line);

#if defined(__GNUC__) && !defined(__ICC) && !defined(__TMS470__)
#if __GNUC__ > 4 || (__GNUC__ == 4 && (__GNUC_MINOR__ > 6 || (__GNUC_MINOR__ == 6 && __GNUC_PATCHLEVEL__ > 0)))
#pragma GCC diagnostic pop
#endif
#endif

#endif
