/* AUTOGENERATED FILE. DO NOT EDIT. */
#ifndef _MOCK_HAL_H
#define _MOCK_HAL_H

#include "unity.h"
#include "stdint.h"
#include "hal.h"

/* Ignore the following warnings, since we are copying code */
#if defined(__GNUC__) && !defined(__ICC) && !defined(__TMS470__)
#if __GNUC__ > 4 || (__GNUC__ == 4 && (__GNUC_MINOR__ > 6 || (__GNUC_MINOR__ == 6 && __GNUC_PATCHLEVEL__ > 0)))
#pragma GCC diagnostic push
#endif
#if !defined(__clang__)
#pragma GCC diagnostic ignored "-Wpragmas"
#endif
#pragma GCC diagnostic ignored "-Wunknown-pragmas"
#pragma GCC diagnostic ignored "-Wduplicate-decl-specifier"
#endif

void mock_hal_Init(void);
void mock_hal_Destroy(void);
void mock_hal_Verify(void);




#define XT_MEMW_Ignore() XT_MEMW_CMockIgnore()
void XT_MEMW_CMockIgnore(void);
#define XT_MEMW_StopIgnore() XT_MEMW_CMockStopIgnore()
void XT_MEMW_CMockStopIgnore(void);
#define XT_MEMW_Expect() XT_MEMW_CMockExpect(__LINE__)
void XT_MEMW_CMockExpect(UNITY_LINE_TYPE cmock_line);
typedef void (* CMOCK_XT_MEMW_CALLBACK)(int cmock_num_calls);
void XT_MEMW_AddCallback(CMOCK_XT_MEMW_CALLBACK Callback);
void XT_MEMW_Stub(CMOCK_XT_MEMW_CALLBACK Callback);
#define XT_MEMW_StubWithCallback XT_MEMW_Stub
#define XT_EXTW_Ignore() XT_EXTW_CMockIgnore()
void XT_EXTW_CMockIgnore(void);
#define XT_EXTW_StopIgnore() XT_EXTW_CMockStopIgnore()
void XT_EXTW_CMockStopIgnore(void);
#define XT_EXTW_Expect() XT_EXTW_CMockExpect(__LINE__)
void XT_EXTW_CMockExpect(UNITY_LINE_TYPE cmock_line);
typedef void (* CMOCK_XT_EXTW_CALLBACK)(int cmock_num_calls);
void XT_EXTW_AddCallback(CMOCK_XT_EXTW_CALLBACK Callback);
void XT_EXTW_Stub(CMOCK_XT_EXTW_CALLBACK Callback);
#define XT_EXTW_StubWithCallback XT_EXTW_Stub
#define xthal_get_ccount_IgnoreAndReturn(cmock_retval) xthal_get_ccount_CMockIgnoreAndReturn(__LINE__, cmock_retval)
void xthal_get_ccount_CMockIgnoreAndReturn(UNITY_LINE_TYPE cmock_line, uint32_t cmock_to_return);
#define xthal_get_ccount_StopIgnore() xthal_get_ccount_CMockStopIgnore()
void xthal_get_ccount_CMockStopIgnore(void);
#define xthal_get_ccount_ExpectAndReturn(cmock_retval) xthal_get_ccount_CMockExpectAndReturn(__LINE__, cmock_retval)
void xthal_get_ccount_CMockExpectAndReturn(UNITY_LINE_TYPE cmock_line, uint32_t cmock_to_return);
typedef uint32_t (* CMOCK_xthal_get_ccount_CALLBACK)(int cmock_num_calls);
void xthal_get_ccount_AddCallback(CMOCK_xthal_get_ccount_CALLBACK Callback);
void xthal_get_ccount_Stub(CMOCK_xthal_get_ccount_CALLBACK Callback);
#define xthal_get_ccount_StubWithCallback xthal_get_ccount_Stub

#if defined(__GNUC__) && !defined(__ICC) && !defined(__TMS470__)
#if __GNUC__ > 4 || (__GNUC__ == 4 && (__GNUC_MINOR__ > 6 || (__GNUC_MINOR__ == 6 && __GNUC_PATCHLEVEL__ > 0)))
#pragma GCC diagnostic pop
#endif
#endif

#endif
