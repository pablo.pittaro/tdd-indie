//--------------------INCLUDES--------------------------------------------------
//------------------------------------------------------------------------------
#include <stdint.h>
#include <string.h>
#include "unity_internals.h"
#include "unity.h"

//--------------------MACROS AND DEFINES----------------------------------------
//------------------------------------------------------------------------------
/// \brief Extraido de unity.c
#define UNITY_FAIL_AND_BAIL   { Unity.CurrentTestFailed  = 1; TEST_ABORT(); }

/// \brief The true condition of the assert generate a TEST_FAIL
#define TRUE__ 1

/// \brief The fatal condition of the assert generate a TEST_FAIL
#define FATAL__ 2

/// \brief The false condition of the assert generate a TEST_FAIL
#define FALSE__ 0

//--------------------INTERNAL DATA DECLARATION---------------------------------
//------------------------------------------------------------------------------
/// \brief String de fallo. Sacado de unity.c
static const char UnityStrFail_[];

/// \brief internal assert state. The reset state is FALSE__
static uint8_t assert_state;

//--------------------INTERNAL FUNCTION DECLARATION-----------------------------
//------------------------------------------------------------------------------
/// \brief Imprime el resultado del assert. Sacado de unity.c
static void UnityTestResultsBegin(const char* file,
                                  const UNITY_LINE_TYPE line);

//--------------------INTERNAL DATA DEFINITION----------------------------------
//------------------------------------------------------------------------------
static const char UnityStrFail_[]                   = "FAIL";

static uint8_t assert_state = FALSE__;

//--------------------EXTERNAL DATA DEFINITION----------------------------------
//------------------------------------------------------------------------------

//--------------------INTERNAL FUNCTION DEFINITION------------------------------
//------------------------------------------------------------------------------
static void UnityTestResultsBegin(const char* file, const UNITY_LINE_TYPE line)
{
    UnityPrint(file);
    UNITY_OUTPUT_CHAR(':');
    UnityPrintNumber((UNITY_INT)line);
    UNITY_OUTPUT_CHAR(':');
    UnityPrint(Unity.CurrentTestName);
    UNITY_OUTPUT_CHAR(':');
}

//--------------------EXTERNAL FUNCTION DEFINITIONS-----------------------------
//------------------------------------------------------------------------------
extern void on_assert_(char const *file, uint32_t line, uint32_t id)
{
	if(assert_state != TRUE__)
	{
		UnityTestResultsBegin(file, line);
		UnityPrint(UnityStrFail_);

		UNITY_FAIL_AND_BAIL;
	}
	else
	{
		assert_state = FALSE__;
		TEST_PASS();
	}
}
//------------------------------------------------------------------------------
extern void on_assert_fatal_(char const *file, uint32_t line)
{
	if(assert_state != FATAL__)
	{
		UnityTestResultsBegin(file, line);
		UnityPrint(UnityStrFail_);

		UNITY_FAIL_AND_BAIL;
	}
	else
	{
		assert_state = FALSE__;
		TEST_PASS();
	}
}
//------------------------------------------------------------------------------
extern void assert_true_(void)
{
	assert_state = TRUE__;
}
//------------------------------------------------------------------------------
extern void assert_fatal_(void)
{
	assert_state = FATAL__;
}
//------------------------------------------------------------------------------
extern void assert_false_(void)
{
	if(assert_state != FALSE__)
	{
        if (assert_state == TRUE__)
        {
            assert_state = FALSE__;

            /* it never returns */
            UNITY_TEST_FAIL(Unity.CurrentTestLineNumber,
                            "Assert was not met!");
        }
        else
        {
            assert_state = FALSE__;

            /* it never returns */
            UNITY_TEST_FAIL(Unity.CurrentTestLineNumber,
                            "Fatal Assert was not met!");
        }

        /** \brief 1st old version
		TEST_FAIL_MESSAGE("Assert was not met!");
        */

        /** \brief 2nd old version
        //UnityTestResultsBegin(Unity.TestFile, Unity.CurrentTestLineNumber);
		//UnityPrint(UnityStrFail_);
		//UNITY_FAIL_AND_BAIL;
        */
	}
}
//------------------------------------------------------------------------------
extern void utest_logging_str(char *str)
{
    uint32_t len;
    uint32_t i;

    len = strlen(str);

    for (i = 0; i < len; i++)
    {
        putchar(str[i]);
    }
}
//------------------------------------------------------------------------------
extern void utest_logging_uint8(uint8_t data)
{
    char str[8];
    uint32_t len;
    uint32_t i;

    sprintf(str, "%d\n", data);

    len = strlen(str);

    for (i = 0; i < len; i++)
    {
        putchar(str[i]);
    }
}
//------------------------------------------------------------------------------
extern void utest_logging_uint16(uint16_t data)
{
    char str[8];
    uint32_t len;
    uint32_t i;

    sprintf(str, "%d\n", data);

    len = strlen(str);

    for (i = 0; i < len; i++)
    {
        putchar(str[i]);
    }
}
//------------------------------------------------------------------------------
extern void utest_logging_uint32(uint32_t data)
{
    char str[20];
    uint32_t len;
    uint32_t i;

    sprintf(str, "%lu\n", data);

    len = strlen(str);

    for (i = 0; i < len; i++)
    {
        putchar(str[i]);
    }
}
//--------------------END OF FILE-----------------------------------------------
