#include "unity.h"
#include "logger_assert.h"

#include "main.h"
#include "mock_led.h"


void setUp(void)
{
}

void tearDownMine()
{
}

void test_main_should_executeTheMainLoop(void)
{
   led_init_Expect();
   
   led_exec_ExpectAndReturn(1);
   led_exec_ExpectAndReturn(1);
   led_exec_ExpectAndReturn(0);
   
    TEST_ASSERT_EQUAL(0, testeable_main());
}



/* DO NOT MODIFY THIS FUNCTION. THIS WAS GENERATED AUTOMATICALLY */
void tearDown()
{
    ASSERT_END__;
    tearDownMine();
}

