#include "unity.h"
#include "uart_driver.h"


/**
 * UART driver
 * 1. Config baudrate, parity, enable fifo 
 * 2.
*/

extern UART_Driver_t uart;

void setUp(void)
{

}

void tearDown(void)
{

}

void test_hardware_registers_areSet()
{
    config_hardware(9600);

    TEST_ASSERT_EQUAL_UINT32(1, uart.FCR);
    TEST_ASSERT_EQUAL_UINT32(1, uart.LCR);
    TEST_ASSERT_EQUAL_UINT32(65, uart.DLL);
    TEST_ASSERT_EQUAL_UINT32(0, uart.DLM);
}

void test_uart_receives_when_LSR_isSet()
{
    char retValue;
    uart.RBR = 0x12;
    uart.LSR = 1;

    retValue = uart_receive_char();

    TEST_ASSERT_EQUAL_CHAR(0x12, retValue);
}

void test_uart_returns_0_when_LSR_isNotSet()
{
    char retValue;
    uart.RBR = 0x12;
    uart.LSR = 0;

    retValue = uart_receive_char();

    TEST_ASSERT_EQUAL_CHAR(0, retValue);
}

void test_uart_sends_char_and_isReceived()
{
    char sentByte = 0x13;
    uart.LSR = 1;
    
    uart_send_char(sentByte);
    uart.RBR = uart.THR;

    TEST_ASSERT_EQUAL_CHAR(sentByte, uart_receive_char());
}