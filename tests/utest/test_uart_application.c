#include "unity.h"
#include "uart_application.h"
#include "mock_uart_driver.h"

/**
 * Requirements
 * 1. Configure driver
 * 2. Sends data stream
 * 3. Receive data stream
*/

void setUp()
{

}

void tearDown()
{

}

void test_uart_isInitialized()
{
    uint32_t baud = 9600;
    config_hardware_Expect(baud);
    uart_init(baud);
}

void test_uart_sendsMessage()
{
    
    uart_send_char_Expect('A');
    send_message("A");
}

void test_uart_send_message_withExpectedStream()
{

    uart_send_char_Expect('T');
    uart_send_char_Expect('D');
    uart_send_char_Expect('D');
    uart_send_char_Expect(' ');
    uart_send_char_Expect('R');
    uart_send_char_Expect('U');
    uart_send_char_Expect('L');
    uart_send_char_Expect('E');
    uart_send_char_Expect('S');

    send_message("TDD RULES");
}

void test_receive_message_returnsChar()
{

    char ch[2];
    uart_receive_char_ExpectAndReturn('A');
    uart_receive_char_ExpectAndReturn('\0');
    receive_message(ch);

    TEST_ASSERT_EQUAL_CHAR('A', ch[0]);
}

void test_receive_message_returnString()
{
    char string[6];

    uart_receive_char_ExpectAndReturn('H');
    uart_receive_char_ExpectAndReturn('E');
    uart_receive_char_ExpectAndReturn('L');
    uart_receive_char_ExpectAndReturn('L');
    uart_receive_char_ExpectAndReturn('O');
    uart_receive_char_ExpectAndReturn('\0');
    receive_message(string);

    TEST_ASSERT_EQUAL_CHAR_ARRAY("HELLO", string, 5);
}

void test_application_DestroysPlannet()
{
    config_hardware_Expect(9600);

    uint32_t expected = 0;
    uart_receive_char_ExpectAndReturn('Y');
    uart_receive_char_ExpectAndReturn('o');
    uart_receive_char_ExpectAndReturn('d');
    uart_receive_char_ExpectAndReturn('a');
    uart_receive_char_ExpectAndReturn('\0');

    uart_send_char_Expect('D');
    uart_send_char_Expect('e');
    uart_send_char_Expect('s');
    uart_send_char_Expect('t');
    uart_send_char_Expect('r');
    uart_send_char_Expect('o');
    uart_send_char_Expect('y');

    expected = application(1);

    TEST_ASSERT_EQUAL_UINT32(0x10000, expected);
}

void test_application_Waits()
{
    config_hardware_Expect(9600);

    uint32_t expected = 0;
    uart_receive_char_ExpectAndReturn('Y');
    uart_receive_char_ExpectAndReturn('o');
    uart_receive_char_ExpectAndReturn('c');
    uart_receive_char_ExpectAndReturn('c');
    uart_receive_char_ExpectAndReturn('\0');

    uart_send_char_Expect('W');
    uart_send_char_Expect('a');
    uart_send_char_Expect('i');
    uart_send_char_Expect('t');
    expected = application(1);

    TEST_ASSERT_EQUAL_UINT32(0, expected);
}

void test_application_TurnsOnShield()
{
    config_hardware_Expect(9600);

    uint32_t expected = 0;
    uart_receive_char_ExpectAndReturn('Y');
    uart_receive_char_ExpectAndReturn('o');
    uart_receive_char_ExpectAndReturn('e');
    uart_receive_char_ExpectAndReturn('c');
    uart_receive_char_ExpectAndReturn('\0');

    uart_send_char_Expect('S');
    uart_send_char_Expect('h');
    uart_send_char_Expect('i');
    uart_send_char_Expect('e');
    uart_send_char_Expect('l');
    uart_send_char_Expect('d');
    uart_send_char_Expect('s');

    expected = application(1);

    TEST_ASSERT_EQUAL_UINT32(0x100, expected);
}

void test_application_TurnsOffEngine()
{
    config_hardware_Expect(9600);

    uint32_t expected = 0;
    uart_receive_char_ExpectAndReturn('Y');
    uart_receive_char_ExpectAndReturn('n');
    uart_receive_char_ExpectAndReturn('n');
    uart_receive_char_ExpectAndReturn('c');
    uart_receive_char_ExpectAndReturn('\0');

    uart_send_char_Expect('E');
    uart_send_char_Expect('n');
    uart_send_char_Expect('g');
    uart_send_char_Expect('i');
    uart_send_char_Expect('n');
    uart_send_char_Expect('e');

    expected = application(1);

    TEST_ASSERT_EQUAL_UINT32(0x1, expected);
}