#ifndef LED_H__
#define LED_H__

#include <stdint.h>

extern void led_init(void);
extern uint8_t led_exec(void);

#endif /* LED_H__ */

