#ifndef UART_APPLICATION_H__
#define UART_APPLICATION_H__


#include <stdint.h>
#include "uart_driver.h"
void uart_init(uint32_t baudrate);
void send_message(char *str);
void receive_message(char *str);
uint32_t application(const uint8_t num);
#endif // UART_APPLICATION_H