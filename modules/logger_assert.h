#ifndef LOGGER_ASSERT_H__
#define LOGGER_ASSERT_H__

//--------------------INCLUDES--------------------------------------------------
//------------------------------------------------------------------------------
#include <stdint.h>
#include <assert.h>
#include <string.h>

//--------------------MACROS Y DEFINES------------------------------------------
//------------------------------------------------------------------------------
#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 :\
                      __FILE__)

#ifdef LOGGER_ASSERT
#define logger_assert(test_) \
	do { \
		if (0 == (test_)) \
        { \
            on_assert_(__FILENAME__, __LINE__, 0); \
        } \
	} while (0)

#define logger_assert_id(id_, test_) 					\
    do { \
        if (0 == (id_)) \
        { \
            on_assert_(__FILE__, __LINE__, 1); \
        } \
        else \
        { \
            if (0 == (test_)) \
            { \
                on_assert_(__FILE__, __LINE__, (id_)); \
            } \
        } \
	} while (0)

#define logger_assert_fatal(test_) \
	do { \
		(test_)? (void)0 : on_assert_fatal_(__FILENAME__, __LINE__);    \
	} while (0)

#define logger_assert_fatal_without_backup(test_, shutdown_, hardfault_) \
    do { \
		if (0 == (test_)) \
        { \
            if (shutdown_) \
            { \
                on_assert_shutdown_(); \
            } \
            if (hardfault_) \
            { \
                on_assert_fatal_without_backup_(__FILENAME__, __LINE__); \
            } \
            else \
            { \
                while (1); \
            } \
        } \
	} while (0)

#else
#define logger_assert(test_) assert(test_)
#define logger_assert_id(id_, test_) assert(test_)

/** \brief Assert without log
*/
#define logger_assert_fatal(test_) assert(test_)

/** \brief Assert without log and without backup
*/
#define logger_assert_fatal_without_backup(test_, shutdown_, hardfault_) \
        assert(test_)
#endif

#ifdef UTEST__
#define ASSERT_TEST__ assert_true_()
#define ASSERT_FATAL__ assert_fatal_()
#define ASSERT_END__ assert_false_()
#endif

//--------------------TYPEDEF---------------------------------------------------
//------------------------------------------------------------------------------

//--------------------DECLARACION DE DATOS EXTERNOS-----------------------------
//------------------------------------------------------------------------------

//--------------------DECLARACION DE FUNCIONES EXTERNAS-------------------------
//------------------------------------------------------------------------------
#ifdef LOGGER_ASSERT
/// \brief
///
/// \param[in] file nombre del archivo donde se ejecuta el assert
/// \param[in] line numero de linea del archivo donde se ejecuta el assert
/// \param[in] id Identificador de error
///
extern void on_assert_(char const *file, uint32_t line, uint32_t id);

extern void on_assert_fatal_(char const *file, uint32_t line);

extern void on_assert_fatal_without_backup_(char const *file, uint32_t line);

extern void on_assert_shutdown_(void);
#endif

#ifdef UTEST__
/// \brief
///
extern void assert_true_(void);

/// \brief
///
extern void assert_fatal_(void);

/// \brief
///
extern void assert_false_(void);
#endif

//--------------------FIN DEL ARCHIVO-------------------------------------------
#endif /* LOGGER_ASSERT_H__ */
