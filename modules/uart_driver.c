#include "uart_driver.h"

UART_Driver_t uart;

void config_hardware(uint32_t baudrate)
{
    uint32_t clock_frequency = 10000000;
    /* Access configuration registers*/
    uart.FCR = 1;    
    uart.LCR = 1;   
    /*Set frequency divisors*/
    uart.DLL = (clock_frequency / (baudrate * 16)) ;
    uart.DLM = 0;   
}

char uart_receive_char()
{
    char received = (char) uart.RBR;
    if(uart.LSR == 0)
    {
        return 0;
    }
    else
    {
        return received;
    }   
}

void uart_send_char(char byte)
{
    uart.THR = byte;
}