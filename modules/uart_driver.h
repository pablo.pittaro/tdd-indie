#ifndef UART_DRIVER_H__
#define UART_DRIVER_H__

#include <stdint.h>

typedef struct{
    uint32_t FCR; // FIFO control Register
    uint32_t LCR; // Line control register
    uint32_t LSR; // Line status register
    uint32_t DLL; // LSB of baudrate generator value
    uint32_t DLM; // MSB of baudrate generator value
    uint32_t THR;  // Data to be transmitted
    uint32_t RBR;  // Data received

} UART_Driver_t;

extern UART_Driver_t uart;
void config_hardware(uint32_t baudrate);
char uart_receive_char();
void uart_send_char(char byte);

#endif // UART_DRIVER_H__