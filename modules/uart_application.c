#include "uart_application.h"


void uart_init(uint32_t baudrate)
{
    config_hardware(baudrate);
}

void send_message(char *str)
{

    while(*str != '\0'){
        uart_send_char(*str);
        str++;
    }
    
}

void receive_message(char *str)
{
    *str = uart_receive_char();
    while(*str != '\0')
    {
        str++;
        *str = uart_receive_char();
    }
}

uint32_t application(const uint8_t num)
{
    char *c;
    char c2[5];
    uint8_t acum1 = 0;
    uint8_t acum2 = 0;
    uint8_t acum3 = 0;
    uint32_t acum = 0;
    int i = 0; 
    uart_init(9600);

    for( i = 0 ; i < num; i++){
        receive_message(c2);
        if(c2[0] == 'Y')
        {
            if((c2[1] == 'o') && (c2[3] != 'b')){
                if(c2[2] == 'd'){
                    c = "Destroy";
                    send_message(c);
                    acum1 += 1;
                }
                else if(c2[2] == 'e')
                {
                    c = "Shields";
                    send_message(c);
                    acum2 += 1;
                }
                else
                {
                    c = "Wait";
                    send_message(c);
                }
            } 
            else if(c2[1] == 'n')
            {       
                c = "Engine";
                send_message(c);
                acum3 += 1;
            }      
        }
    }

    acum = (((acum1<<16)&0xFF0000)|((acum2<<8)&0xFF00)|(acum3&0xFF)); 
    return acum;
}
