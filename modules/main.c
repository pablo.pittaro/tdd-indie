#include "led.h"

#ifndef UTEST__
int main(void)
#else
int testeable_main(void)
#endif
{
    led_init();
    
    while(led_exec())
    { 
    };

    return 0;
}

