#include <stdio.h>
#include <time.h>
#include "led.h"

#define LED_OFF 1
#define LED_ON 2

static uint8_t state;

extern void led_init(void)
{
    state = LED_OFF;
}

extern uint8_t led_exec(void)
{
    struct timespec nsecs = {0, 500000000L};
    struct timespec nsecs2;

    if (LED_OFF == state)
    {
        printf("Blinking-OFF\n");
        state = LED_ON;
    }
    else
    {
        printf("Blinking-ON\n");
        state = LED_OFF;
    }
    
    nanosleep(&nsecs, &nsecs2);

    return 1;
}
