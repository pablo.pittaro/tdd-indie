# TDD repository


## Overview
This project is for training purposes. 
It's the code used in the TDD for embedded systems in SASE 2023, **insert link to presentation**

## Development

Create new module 
`ceedling module:create[newModule]`
### Test

#### Test:
`ceedling clean test:all`

or just:

`ceedling`
